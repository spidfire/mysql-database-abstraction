<?php


abstract class CasualModel{
	var $table = null;
	//name, default, value, originalValue, is_changed, type
	var $fields = array();
	var $pkField = null;
	var $error = array();
	private $used_search = null;

	// linkname => array("{type}","{from}","{to}","{class}")
	// linkname => array("tomany","id","id","users")
	// linkname => array("toone","id","id","flabber")
	var $links = array();


	// if search == null there is a new object
	final function __construct($search=null){return ( include("includes/construct.php") );}
	public function store($throwOnError=true,$forceinsert=false){return ( include("includes/store.php") );}
	public function getValue($name){return ( include("includes/getFunction.php") );	}
	public function setValue($name,$value,$is_init=false){return ( include("includes/setFunction.php") );	}
	public function __get($name){return $this->getValue($name);	}
	public function __set($name,$value){return $this->setValue($name,$value);	}
	public function __isset($name){return ( include("includes/isset.php") );}
	public function is_valid(){return count($this->error) == 0;	}
	public function __toString(){ return ( include("includes/tostring.php") ); }
	public function getLink($name){ return ( include("includes/getLink.php") );	}
	public function setLink($name,$value){return ( include("includes/setLink.php") );	}
	public function delete(){ DB::delete($this->getTableName(),$this->pkField."=%d",$this->used_search);	}
	public function pk(){ return $this->used_search;}


	//events
	public function pre_insert(){}
	public function post_insert(){}
	public function pre_update(){}
	public function post_update(){}


	function formatTableName($table) {
	    $table = str_replace('`', '', $table);
	    if (strpos($table, '.')) {
	      list($table_db, $table_table) = explode('.', $table, 2);
	      $table = "`$table_db`.`$table_table`";
	    } else {
	      $table = "`$table`";
	    }

	    return $table;
	  }

	 function printShort(){
		$return = array();
		foreach($this->fields as $name => $type){
			$value = $type['value'] == null ? "null" : htmlspecialchars($type['value']);
			if($type['type'] == "text"){
				$return[] = substr(htmlspecialchars($value),0,20)."....";
			}else{
				$return[]  = $value;
			}
		}
		return implode(" - ", $return);

	 }
	final function getAllFields(){
		$items = array();
		$items[] =  $this->formatTableName($this->pkField);
		foreach($this->fields as $field){
			$items[] = $this->formatTableName($field['name']);
		}
		return implode(",", $items);
	}


	public static function getAllQuery( $where =null,$limit=null){
		$objectDetails = new static();
		$whereStatement = $where != null ? DB::sqleval("WHERE %l",$where)->text : "";
		$table = $objectDetails->getTableName();
		$d = DB::sqleval("Select %l FROM %b  %l",$objectDetails->getAllFields(),$table,$whereStatement);
		$sql = $d->text;
		if($limit !== null){
			$sql .= " Limit ".$limit;
		}
		return $sql;
	}


	public static function getAll( $where =null,$limit=null){
		$sql = self::getAllQuery($where,$limit);
		return self::queryToClasses($sql);
	}
	static function queryToClasses($query){
		$result = DB::query($query);
		$ret = array();
		foreach($result as $res){
			$ret[] = new static($res);
		}
		return $ret;

	}

	function getKeyValue(){
		$items = array();
		foreach($this->fields as $field){
			$items[$field['name']] = $field['value'];
		}
		return $items;
	}
	function getTableName(){
		return $this->formatTableName($this->table);
	}

	function getUpdatedFields($forceall=false){
		$updatedFields = array();
		foreach($this->fields as $name => $field){
			if($field['is_changed'] == true || $forceall){
				$updatedFields[$field['name']] = $field['value'];
			}
		}
		return $updatedFields;
	}
	function import($data,$ignore_unkown_fields= false){
		foreach($data as $name => $value){
			$lower = strtolower($name);
			if($lower == strtolower($this->pkField)){
				$this->used_search = $value;
			}elseif(isset($this->fields[$lower])){
				$this->setValue($name,$value);
			}elseif($ignore_unkown_fields == false){
				throw new Exception("Unknown field found in the (array)insert: ".$lower."! table: '".$this->table."'", 1);

			}

		}

	}

	function customExport($array){
		//if you exend this you can controll what will get exported!
		return $array;
	}

	// If you set the whitelist you only will show the given fields
	function export($whitelist=null){
		$fields = array();
		if($this->used_search != null){
			if($whitelist === null || in_array($this->pkField, $whitelist)){
				$fields[$this->pkField] = $this->used_search;
			}

		}
		foreach($this->fields as $name => $field){
			if($whitelist === null || in_array($field['name'], $whitelist)){
				if($field['type'] == 'json' && is_string($field['value'])){
					if(empty($field['value'])){
						$fields[$field['name']] = array();
					}else{
						$fields[$field['name']] = json_decode($field['value'],true);
						
					}
				}else{
					$fields[$field['name']] = $field['value'];
				}
			}
		}


		return $this->customExport($fields);
	}


	// Error
	public static function exportAll($where =null, $limit=null, $whitelist=null){
		$sql = self::getAllQuery($where, $limit);
		$result = DB::query($sql);
		return self::parseForExport($result, $whitelist);
	}
	// If you set the whitelist you only will show the given fields
	public static function exportToArray(array $array, $whitelist=null){
		$export = array();
		foreach ($array as $key => $value) {
			$export[] = $value->export($whitelist);
		}
		return $export;
	}

	public static function parseForExport(array $array,$whitelist=null){
		$ret = array();
			$temp = new static();
		foreach($array as $res){
			$temp->import($res,true);
			$ret[] = $temp->export($whitelist);
		}
		return $ret;
	}

	function copy(){
		$class = get_class($this);
		$copy = new  $class();
		$copy->import($this->getKeyValue());
		return $copy;
	}

}