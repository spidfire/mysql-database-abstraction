<?php

	class CasualModel_Overview{
		var $model;
		function __construct($class){
			//is an child of CasualModel
			if(!in_array('CasualModel', class_parents ($class))){
				throw new Exception("The class is no child of CasualModel", 1);
			}
			$this->model = new $class();


		}
		function addButton($name,$link){
			$this->editField[$name] = $link;
		}

		var $hideFields = array();
		var $editField = array();
		var $tableattr = array();
		function getHTML($data){
			$xml = new SimpleXMLElement('<xml/>',LIBXML_NOENT  );
			$table = $xml->addChild('table');
			foreach ($this->tableattr as $key => $value) {
				$table->addAttribute($key,$value);
			}
			$thead = $table->addChild('thead');
			$this->head($thead);

			$tbody = $table->addChild('tbody');
			$this->body($tbody,$data);
			return $xml->table->asXML();
		}


		function head($thead){
			$row = $thead->addChild('tr');
			if(count($this->editField) != 0){
				$row->addChild("th","#");
			}
			foreach($this->model->fields as $field){
				if(!in_array($field['name'], $this->hideFields)){
					$row->addChild("th",$field['name']);
				}
			}
		}

		function body($tbody,$data){
			foreach ($data as $key => $value) {
				$row = $tbody->addChild('tr');
				if(count($this->editField) != 0){
					$rowdata = $row->addChild("td");
					foreach ($this->editField as $name => $link) {
						$a = $rowdata->addChild("a",$name);
						$newlink = $link;
						preg_match_all("/\{(\w+)\}/", $newlink, $results,PREG_SET_ORDER);
						foreach ($results as $tag) {
							$newlink = str_replace($tag[0], $value->getValue($tag[1]),$newlink );
						}

						$a->addAttribute("href",$newlink);
					}

				}
				foreach($this->model->fields as $field){
					if(!in_array($field['name'], $this->hideFields)){
						$this->format($row,$value,$field);
					}
				}
			}
		}

		function format($row,$value,$field){
			if($field['type'] == 'textarea' || substr($field['type'],0,7) == 'varchar'){
				$data = $value->getValue($field['name']);
				if(strlen($data) > 30){
					$data = substr($data,0,30)."...";
				}
				$row->addChild("td",$data);
			}elseif(substr($field['type'],0,3) == 'int'){
				$row->addChild("td","number:".$value->getValue($field['name']));
			}else{
				$row->addChild("td",$value->getValue($field['name']));
			}
		}



	}