<?php

class CasualModel_Update{
	var $model;
	function __construct($class){
		//is an child of CasualModel
		if(!in_array('CasualModel', class_parents ($class))){
			throw new Exception("The class is no child of CasualModel", 1);
		}
		$this->model = $class;
	}

	var $hideFields = array();
	var $method = "POST";
	var $action = null;
	function getHTML($id=null){
		$xml = new SimpleXMLElement('<xml/>'  );
		$form = $xml->addChild('form');
		if($this->method != null)
			$form->addAttribute("method",$this->method);
		if($this->action != null)
			$form->addAttribute("action",$this->action);

		// Model part
		$model = new $this->model($id);

		if(isset($_REQUEST['saveCasualForm'])){
			$model->import($_REQUEST['casualsave']);
			if($model->is_valid()){
				$model->store();
				$form->addChild("div","SAVED");
			}else{
				$form->addChild("div",implode("<br/>", $model->error));
			}

		}



		$table = $form->addChild('table');
		foreach($model->fields as $field){
			if(!in_array($field['name'], $this->hideFields)){
				$row = $table->addChild("tr");
				$row->addChild("td",$field['name']);
				$valuecontainer = $row->addChild("td");
				$this->createInput($valuecontainer,$field);
			}
		}
		$row = $table->addChild("tr");
			$row->addChild("td");
		$submit = $row->addChild("td")->addChild("input");
		$submit->addAttribute("value","save");
		$submit->addAttribute("type","submit");
		$submit->addAttribute("name","saveCasualForm");
		return $xml->form->asXML();

	}

	function createInput($container,$field){
		if(false){

		}else{
			$input = $container->addChild("input");
			$input->addAttribute("name","casualsave[".$field['name']."]");
			$input->addAttribute("id","save".$field['name']."");
			$input->addAttribute("value",$field['value']);
		}
	}
}