<?php
include "../MeekroDB.php";
DB::$user = 'root';
DB::$password = '';
DB::$dbName = 'test';

//include "model.php";
include("../lib/model.php");
// require("../compile.php");
// include("../casualmodel.php");
include("customer.php");
include("shoppingcart.php");
include("birthplaces.php");

require_once('simpletest/autorun.php');

class ModelBasicTest extends UnitTestCase {
	function setUp() {
        DB::query("TRUNCATE TABLE birthplaces");
        DB::query("TRUNCATE TABLE customer");
        DB::query("TRUNCATE TABLE shoppingcart");
    }

    function testIfTableEmpty() {

        $this->assertEqual(count(CustomerModel::getAll()), 0);
        // insert 1
        $test = new CustomerModel();
        $test->name = "Test person";
        $test->store();

        $this->assertEqual(count(CustomerModel::getAll()), 1);

        //update after storing it once
        $test->name = "Test gangsta";
        $test->store();

        $this->assertEqual(count(CustomerModel::getAll()), 1);
        // the inserted primairy key is 1
        $this->assertEqual($test->id, 1);

        //get the object 1
        $lol = new CustomerModel(1);
        $this->assertEqual( $lol->name, "Test gangsta");
        $this->assertNotEqual( $lol->creation, 0 );
        $this->assertNotEqual( $lol->update, 0 );
    }

    function testLinks(){

        //single
    	$this->assertEqual(count(ShoppingcartModel::getAll()), 0);


        $bi = new BirthplacesModel();
        $bi->name = "Utrecht";
        $bi->country = "Netherlands";

        $t = new CustomerModel();
        $t->linkBirthplace = $bi;
        //$t->linkCart = new ShoppingcartModel();

        $t->store();
        $this->assertEqual( $t->birthplace, 1);
        $this->assertEqual( $t->linkBirthplace->name, "Utrecht");

        $lastinsert = $t->id;

        $nt = new CustomerModel($lastinsert);
        $this->assertEqual( $t->linkBirthplace->name, "Utrecht");

        //multi

        $nt = new CustomerModel();

        $items = array();
        $i = new ShoppingcartModel();
        $i->products = 22;
        $i->amount = 1;
        $items[] = $i;
        $i = new ShoppingcartModel();
        $i->products = 23;
        $i->amount = 1;
        $items[] = $i;
        $nt->store();
        $nt->linkcart = $items;
        $this->assertEqual( count($nt->linkcart), 2);
        $this->assertEqual( $nt->linkcart[1]->products, 23);

        $a = $nt->linkcart;
        $a[1]->products = 25;
        $a[1]->store();
        $nt->linkcart = $a;

        $this->assertEqual( count($nt->linkcart), 2);
        $this->assertEqual( $nt->linkcart[1]->products, 25);
    }
}