<?php

// This class has been generated using the generate.php
class ShoppingcartModel extends CasualModel{
	var $table = 'shoppingcart';
	var $pkField = 'id';
	var $links = array(
		'page' => array("toone","user","id","CustomerModel"),
		//'page' => array("tomany","pageid","id","pageModel"),
	);
	var $fields = array(
		'user' => array('type' => 'int(11)'),
		'products' => array('type' => 'int(11)'),
		'amount' => array('type' => 'int(11)')
	);
	function pre_update(){}
	function pre_insert(){}
	function post_insert(){}
	function post_update(){}
}