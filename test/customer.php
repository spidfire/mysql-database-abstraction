<?php

// This class has been generated using the generate.php
class CustomerModel extends CasualModel{
	var $table = 'customer';
	var $pkField = 'id';
	var $links = array(
		'birthplace' => array("toone","birthplace","id","BirthplacesModel"),
		'cart' => array("tomany","id","user","ShoppingcartModel"),
	);
	var $fields = array(
		'name' => array('type' => 'varchar(128)'),
		'email' => array('type' => 'varchar(256)'),
		'creation' => array('type' => 'int(11)'),
		'birthplace' => array('type' => 'int(11)'),
		'update' => array('type' => 'int(11)')
	);
	function pre_update(){
		$this->update = time();
	}
	function pre_insert(){
		$this->creation = time();
	}
	function post_insert(){}
	function post_update(){}
}