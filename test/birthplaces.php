<?php

// This class has been generated using the generate.php
class BirthplacesModel extends CasualModel{
	var $table = 'birthplaces';
	var $pkField = 'id';
	var $links = array(
		//'page' => array("toone","pageid","id","pageModel"),
		'page' => array("tomany","id","birthplace","CustomerModel"),
	);
	var $fields = array(
		'name' => array('type' => 'varchar(32)'),
		'country' => array('type' => 'varchar(32)')
	);
	function pre_update(){}
	function pre_insert(){}
	function post_insert(){}
	function post_update(){}
}



$a = new BirthplacesModel();

$a->name = "djurre";

$a->store();

$p = array();
$p[] = new CustomerModel(2);
$a->linkPage = $p;