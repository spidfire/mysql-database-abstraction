<?php

include "../MeekroDB.php";
DB::$user = 'root';
DB::$password = '';
DB::$dbName = 'test';

include("../lib/model.php");
include("../extention/casual_overview.php");
include("../extention/casual_update.php");
include("customer.php");

if(isset($_GET['edit'])){
	echo "<h2>EDIT</h2>";
	$edit = new CasualModel_Update('CustomerModel');
	echo $edit->getHTML($_GET['edit']);

}else{
	echo "<h2>NEW</h2>";
	$edit = new CasualModel_Update('CustomerModel');
	echo $edit->getHTML();
}


$overview = new CasualModel_Overview('CustomerModel');
$overview->addButton("edit","?edit={id}");
$overview->hideFields = array("creation");
echo $overview->getHTML(CustomerModel::getAll());



