<?php


abstract class CasualModel{
	var $table = null;
	//name, default, value, originalValue, is_changed, type
	var $fields = array();
	var $pkField = null;
	var $error = array();
	private $used_search = null;

	// linkname => array("{type}","{from}","{to}","{class}")
	// linkname => array("tomany","id","id","users")
	// linkname => array("toone","id","id","flabber")
	var $links = array();


	// if search == null there is a new object
	final function __construct($search=null){
#--------start file includes/construct.php 

//table not set
if($this->table == null){
	throw new Exception("No table has been defined", 1);			
}
//fields not set
if(!is_array($this->fields) || count($this->fields) == 0){
	throw new Exception("No proper fields are set", 1);			
}
//pk not set
if($this->pkField == null){
	throw new Exception("No proper primairyKey has been defined", 1);			
}
$newFields = array();
foreach($this->fields as $name => $type){
	$empty['name'] = $name; 
	$empty['default'] = null; 
	$empty['value'] = null; 
	$empty['originalValue'] = null; 
	$empty['is_changed'] = null; 
	$empty['type'] = null; 
	$newFields[strtolower($name)] = array_merge($empty,$type);
}

$this->fields = $newFields;
if($search == null){
	//insert new record
	
}elseif(is_array($search)){
	
	foreach($search as $name => $value){
		$lower = strtolower($name);
		if(isset($this->fields[$lower])){
			$this->setValue($name,$value,true);
		}elseif(strtolower($this->pkField) == $lower){
			$this->used_search = $value;
		}else{
			throw new Exception("Unknown field found in the (array)insert: ".$lower."! table: '".$this->table."'", 1);

		}
		
	}
	if($this->used_search == null){
		throw new Exception("Primairy Key has not been found in the resultset ".__CLASS__, 1);
		
	}

}elseif(ctype_digit((string)$search)){
	$this->used_search = $search;
	$row = DB::queryFirstRow("select * from `".$this->table."` where `".$this->pkField."` = %d",$search);
	if($row != false){
		foreach($row as $name => $value){
			$this->setValue($name,$value,true);
		}
	}else{
		throw new Exception("Selected record(".$search.") is not found in '".$this->table."'! table: '".$this->table."'", 1);
		
	}

}elseif($search instanceof WhereClause){
	$row = DB::query("select * from `".$this->table."` where ".$search->text());
	if(count($row) == 1){
		foreach($row[0] as $name => $value){
			$lower = strtolower($name);
			if(isset($this->fields[$lower])){
				$this->setValue($name,$value,true);
			}elseif(strtolower($this->pkField) == $lower){
				$this->used_search = $value;
			}else{
				throw new Exception("Unknown field found in the (where) insert: ".$lower."! table: '".$this->table."'", 1);

			}
		}
	}elseif(count($row) > 1){
		throw new Exception("The used select statement returned to many rows!  table: '".$this->table."'", 1);
		
	}else{
		throw new Exception("Selected where record(".$search.") is not found in '".$this->table."'! table: '".$this->table."'", 1);
		
	}

}else{
	throw new Exception("Unknown action to open ".gettype($search), 1);
}
#--------end file includes/construct.php 
}
	public function store($throwOnError=true,$forceinsert=false){
#--------start file includes/store.php 

$updatedFields = array();







if($this->used_search == null || $forceinsert == true){
	//insert
	if($this->pre_insert() !== false){
		$updatedFields = $this->getUpdatedFields($forceinsert);
		if(count($updatedFields) == 0){

			$this->error[] = "There are no fields changed";
		}else{
			try{
				DB::insert($this->table,$updatedFields);
				$this->used_search = DB::insertId();
			}catch(Exception $e){
				if($throwOnError == true){
					throw new Exception("DB error: ".$e->getMessage(). " on ".$e->getFile().":".$e->getLine(), 1);
				}
				$this->error[] = $e->getMessage(). " on ".$e->getFile().":".$e->getLine();
			}
		}
		$this->post_insert();
		return $this->used_search;
	}else{
		$this->error[] = "the update has been canceled from the pre_insert";
	}
}else{
	if($this->pre_update() !== false){
		$updatedFields = $this->getUpdatedFields($forceinsert);

		//update
		if(count($updatedFields) == 0){

			$this->error[] = "There are no fields changed";
		}else{
			try{
				DB::update($this->table,$updatedFields,$this->pkField." = %d",$this->used_search);
			}catch(Exception $e){
				if($throwOnError == true){
					throw new Exception("DB error: ".$e->getMessage(). " on ".$e->getFile().":".$e->getLine(), 1);
				}
				$this->error[] = $e->getMessage(). " on ".$e->getFile().":".$e->getLine();
			}
		}
		$this->post_update();
		return $this->used_search;
	}else{
		$this->error[] = "the update has been canceled from the pre_update";
	}

}


#--------end file includes/store.php 
}
	public function getValue($name){
#--------start file includes/getFunction.php 

$return = null;
$nameLower= strtolower($name);


if(isset($this->fields[$nameLower])){		

	return $this->fields[$nameLower]['value'];
	

}elseif($nameLower == $this->pkField){
	return $this->used_search;
	

}elseif(substr($nameLower,0,4) == "link" && isset($this->links[substr($nameLower,4)])){
	return $this->getLink(substr($nameLower,4));
	

}else{
	throw new Exception("Get Field does not exist: ".$nameLower, 1);
	
}
#--------end file includes/getFunction.php 
	}
	public function setValue($name,$value,$is_init=false){
#--------start file includes/setFunction.php 

$nameLower= strtolower($name);
if(isset($this->fields[$nameLower])){
	//some modifications per type
	if($is_init == true){
		$this->fields[$nameLower]['value'] = $value;
		$this->fields[$nameLower]['originalValue'] = $value;
	}elseif($this->fields[$nameLower]['value'] != $value) {
		$type = $this->fields[$nameLower]['type'];
		if($type == 'date' and ctype_digit($value)){ // Is a timestamp
			$this->fields[$nameLower]['value'] = date("Y-m-d",$value);
			$this->fields[$nameLower]['is_changed'] = true;
		}if($type == 'datetime' and ctype_digit($value)){ // Is a timestamp
			$this->fields[$nameLower]['value'] = date("Y-m-d H:i:s",$value);
			$this->fields[$nameLower]['is_changed'] = true;
		}else{
			$this->fields[$nameLower]['value'] = $value;
			$this->fields[$nameLower]['is_changed'] = true;
		}
	}else{
		// shill the same
	}


}elseif(substr($nameLower, 0,4) == "link"){
	$linkname = substr($nameLower,4);
	return $this->setLink($linkname,$value);

}elseif($nameLower == $this->pkField){
	// ignore pk

}else{
	$this->error[] = "Set Field does not exist: ".$nameLower;
}
#--------end file includes/setFunction.php 
	}
	public function __get($name){return $this->getValue($name);	}
	public function __set($name,$value){return $this->setValue($name,$value);	}
	public function __isset($name){
#--------start file includes/isset.php 


$nameLower= strtolower($name);


if(isset($this->fields[$nameLower])){		
	return true;
}elseif($nameLower == $this->pkField){
	return true;
}else{
	return false;	
}
#--------end file includes/isset.php 
}
	public function is_valid(){return count($this->error) == 0;	}
	public function __toString(){ 
#--------start file includes/tostring.php 


$text = "<b>Table</b>: ".$this->table."<br/>\n";
$text .= "<b>PK</b>: ".$this->pkField."<br/>\n";
$text .= "<b>Fields</b></b>: <br/>\n";
foreach($this->fields as $name => $type){
	$value = $type['value'] == null ? "IS EMPTY" : "<u>".$type['value']."</u>";
	$upd = $type['is_changed'] == true ? " (has been changed)" : "";
	$text .= "-<b>". $type['name']."</b>(". $type['type']."): ";
	if($type['type'] == "text"){
		$text .= "&nbsp;&nbsp;".$upd."<br/>\n".substr(htmlspecialchars($value),0,100)."....<br/>\n";
	}else{
		$text .= "".$value."".$upd."<br/>\n";
	}
}

$text .= "<b>has_errors</b>: ".count($this->error)."<br/>\n";
foreach ($this->error as $key => $value) {
	$text .= "-$key: ".$value."<br/>\n";
}

return $text;


#--------end file includes/tostring.php 
 }
	public function getLink($name){ 
#--------start file includes/getLink.php 


$name = strtolower($name);
if(isset($this->links[$name])){
	$type = strtolower($this->links[$name][0]);
	$from = $this->links[$name][1];
	$to = $this->links[$name][2];
	$class = $this->links[$name][3];
	if($type == 'tomany'){
		// select many from the other table
		$where = new WhereClause("and");
		$fromValue = $this->getValue($from);
		$where->add($to." = %d",$fromValue);
		$tmp = new $class();

		$result = DB::query("Select * from ".$tmp->getTableName()." where ".$where->text());
		$ret = array();
		foreach($result as $res){
			$ret[] = new  $class($res);
		}
		return $ret;
	}elseif($type == 'toone'){
		// select one from the other table
		$where = new WhereClause("and");
		$fromValue = $this->getValue($from);
		$where->add($to." = %d",$fromValue);
		$this->links[$name]['item'] = new  $class($where);
		return $this->links[$name]['item'];
	}else{
		throw new Exception("Foulthy link type is set", 1);

	}


}else{
	throw new Exception("Link is not found", 1);

}
#--------end file includes/getLink.php 
	}
	public function setLink($name,$value){
#--------start file includes/setLink.php 

	if(isset($this->links[$name])){
		$type = strtolower($this->links[$name][0]);
		$from = $this->links[$name][1];
		$to = $this->links[$name][2];
		$class = $this->links[$name][3];
		if($type == 'tomany'){
			if(is_array($value)){
				$items = $this->getLink($name);
				foreach($items as $i){
					$i->delete();
				}
				$left = $this->getValue($from);
				foreach($value as $i){
					$i->setValue($to,$left);
					$i->store(true,true);
				}
			}else{
				throw new Exception("Was expecting an array", 1);
			}
		}elseif($type == 'toone'){
			//toone is right to left
			// select one from the other table
			if($value instanceof $class){
				$value->store();
				$right = $value->getValue($to);
				$this->setValue($from,$right);
			}else{
				throw new Exception("You need to put an instance of the right class here!", 1);
			}
		}else{
			throw new Exception("Foulthy link type is set", 1);

		}
	}else{
		$this->error[] = "Unknown link found: ".$nameLower;
	}


#--------end file includes/setLink.php 
	}
	public function delete(){ DB::delete($this->getTableName(),$this->pkField."=%d",$this->used_search);	}
	public function pk(){ return $this->used_search;}


	//events
	public function pre_insert(){}
	public function post_insert(){}
	public function pre_update(){}
	public function post_update(){}


	function formatTableName($table) {
	    $table = str_replace('`', '', $table);
	    if (strpos($table, '.')) {
	      list($table_db, $table_table) = explode('.', $table, 2);
	      $table = "`$table_db`.`$table_table`";
	    } else {
	      $table = "`$table`";
	    }

	    return $table;
	  }


	final function getAllFields(){
		$items = array();
		$items[] =  $this->formatTableName($this->pkField);
		foreach($this->fields as $field){
			$items[] = $this->formatTableName($field['name']);
		}
		return implode(",", $items);
	}
	public static function getAll( $where =null){
		$objectDetails = new static();
		$whereStatement = $where != null ? " where ".$where->text() : "";
		$table = $objectDetails->getTableName();
		$result = DB::query("Select ".$objectDetails->getAllFields()." from ".$table ." ".$whereStatement);
		$ret = array();
		foreach($result as $res){
			$ret[] = new static($res);
		}
		return $ret;
	}
	function getKeyValue(){
		$items = array();
		foreach($this->fields as $field){
			$items[$field['name']] = $field['value'];
		}
		return $items;
	}
	function getTableName(){
		return $this->formatTableName($this->table);
	}

	function getUpdatedFields($forceall=false){
		$updatedFields = array();
		foreach($this->fields as $name => $field){
			if($field['is_changed'] == true || $forceall){
				$updatedFields[$field['name']] = $field['value'];
			}
		}
		return $updatedFields;
	}
	function import($data,$ignore_unkown_fields= false){
		foreach($data as $name => $value){
			$lower = strtolower($name);
			if(isset($this->fields[$lower])){
				$this->setValue($name,$value);
			}elseif($ignore_unkown_fields == false){
				throw new Exception("Unknown field found in the (array)insert: ".$lower."! table: '".$this->table."'", 1);

			}

		}

	}
	function copy(){
		$class = get_class($this);
		$copy = new  $class();
		$copy->import($this->getKeyValue());
		return $copy;
	}

}